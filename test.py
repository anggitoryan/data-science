#%%
from sklearn.ensemble import randomforestclassifier
from cProfile import label
from doctest import testfile
from math import degrees
from pyexpat import model
from traceback import print_tb
from xml.etree.ElementInclude import include
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from sklearn import pipeline
import mysql.connector
#%%
db = mysql.connector.connect(

)
#%%
mycursor = db.cursor()
#%%
mycursor.execute('SHOW databases')
#%%
filename = "auto.csv"

headers = ["symboling","normalized-losses","make","fuel-type","aspiration", "num-of-doors","body-style",
         "drive-wheels","engine-location","wheel-base", "length","width","height","curb-weight","engine-type",
         "num-of-cylinders", "engine-size","fuel-system","bore","stroke","compression-ratio","horsepower",
         "peak-rpm","city-mpg","highway-mpg","price"]

df = pd.read_csv(filename, names = headers)
#%%
"""
Pre-Processing Data
"""
#%%
#1. Ganti Missing Value
#%%
#1.1 Cek Data
print(df.dtypes)
print(df.head())
#%%
#1.2 Ganti ? dengan Nan
df = df.replace('?',np.NaN)
#%%
#1.3 Ganti kolom yang salah tipe menjadi float
change_column = ['normalized-losses', 'bore','stroke','horsepower','peak-rpm','price']
for col in change_column :
    df[col] = df[col].astype('float')
#%%
#1.4 Cari Kolom apa aja yang punya missing value
print(df.isnull().sum())
#%%
#1.5 Ganti missing value (float/int) dengan mean
for col in change_column[:-1] :
    mean = df[col].mean()
    df[col] = df[col].replace(np.NaN, mean)
print(df.isnull().sum())
#%%
#1.6 Ganti Missing value (categorical) dengan mode
print(df['num-of-doors'].value_counts())
max_number = df['num-of-doors'].mode()[0]
df['num-of-doors'] = df['num-of-doors'].fillna(max_number)
#%%
#1.7 Menghilangkan baris yang memiliki missing column (Target)
df = df.dropna(axis=0, how = 'any')
print(df.isnull().sum())
#%%
#2. Ganti Format
#Sudah ketika ngubah data type nya
#%%
#3. Normalisasikan Datanya
col_normalize = ['length','width','height']
print("Before : \n", df[col_normalize])
for col in col_normalize:
    df[col] = df[col]/df[col].max()
print("After : \n", df[col_normalize])
#%%
#4. Binning Data-nya
bins = np.linspace(min(df['price']),max(df['price']),4)
group_name = ["Low","Medium","High"]
df["price_binned"] = pd.cut(df['price'],bins,labels = group_name,include_lowest=True)
print(df.head())
#%%
#5. Mengubah Categorical jadi Numerical
pd.get_dummies(df['fuel-type'])

#%%
"""
Exploratory Data Analysis
"""
#%%
#1 Menjelajahi setiap variabel (dtypes)
df.dtypes
#%%
#2 Descriptive Statistics (Describe, value_counts)
#2.1 Describe
print(df.describe())
#%%
#2.2 value_counts (Untuk mencari apakah penyebaran intravariabel itu rata)
column_object = df.select_dtypes(include=['object'])
for column in column_object:
    print(df[column].value_counts().to_frame())
#%%
#3 Group By (groupby,pivot,heatmap)
#3.1 GroupBy (untuk Target dan variable Categorical)
group1 = df.groupby(['make','num-of-doors'],as_index=False)['price'].mean()
#%%
#3.2 Pivot
pivot_group1 = group1.pivot(index = 'make',columns ='num-of-doors')
print(pivot_group1)
pivot_group1 = pivot_group1.fillna(0)
#%%
#3.3 Heatmap
sns.heatmap(pivot_group1, cmap = 'RdBu')
#%%
#4 Correlation(pearson, corr, regplot, boxplot)
#%%
#4.1 Regplot
column_float = df.select_dtypes(['float','int']).columns.tolist()
print(column_float)
for column in column_float[:-1]:
    plt.figure()
    sns.regplot(x = column,y = 'price', data = df)
#%%
#4.2 Barplot
column_obj = df.select_dtypes('object').columns.tolist()
for column in column_obj:
    plt.figure()
    sns.boxplot(x=column,y = 'price',data = df)
#%%
from scipy import stats
#4.3 Corr
df.corr()
pearson_coef, p_value = stats.pearsonr(df['wheel-base'],df['price'])
print(pearson_coef)
print(p_value)
#5 Stats-Correlation
# %%
#Model Deployment
#1. Linear Regression
#1.1 Simple Linear Regression
from sklearn.linear_model import LinearRegression
LR0 = LinearRegression()
X = df[['horsepower']]
Y = df['price']
LR0.fit(X,Y)
prediction = LR0.predict(X)
print(LR0.coef_)
print(LR0.intercept_)
#%%
#1.2 Multiple Linear Regression
LR1 = LinearRegression()
Z = df[['horsepower','highway-mpg','engine-size','curb-weight']]
Y = df['price']
LR1.fit(Z,Y)
MLR_Prediction = LR1.predict(Z)
print(LR1.coef_)
print(LR1.intercept_)
#%%
#2. Evaluasikan model (Visualisasikan) 
#2.1 Regplot dan Residplot (Simple)
sns.regplot(x = prediction, y = Y, data = df)
plt.figure()
sns.residplot(x=prediction,y=Y, data = df)
#%%
#2.2 Displot (Multiple)
ax1 = sns.kdeplot(x=MLR_Prediction,color='b')
sns.kdeplot(x= Y, ax=ax1, color = 'c')
#%%
#3. Polynomial
#3.1 Simple Polynomial
X = df['horsepower'].astype('int')
Y = df['price'].astype('int')
f = np.polyfit(X,Y,2)
p = np.poly1d(f)
myline = np.linspace(min(X),max(X),df.shape[0])
plt.scatter(X,Y)
plt.plot(myline, p(myline))
plt.show()
#%%
#3.2 Multiple Polynomial
from sklearn.preprocessing import PolynomialFeatures
Y = df['price']
PF = PolynomialFeatures(degree=2)
Poly = PF.fit_transform(Z)
#%%
#4. Pipeline
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
Input =[('scale',StandardScaler()),('polynomial',PolynomialFeatures(include_bias=False)),('regression',LinearRegression())]
pipe = Pipeline(Input)
pipe.fit(Z,Y)
prediction = pipe.predict(Z)
#%%
#5. Evaluasi model (nominal)
from sklearn.metrics import mean_squared_error, r2_score
#5.1 R-Squared
print(r2_score(y_true= Y, y_pred=prediction))
#%%
#5.2 MSE
mean_squared_error(y_true= Y, y_pred=prediction)
# %%
#Model Evaluation
#1. Cek apakah model berfungsi dengan baik dengan data baru
#1.1 Train_Test_Split
x_data = Z
y_data = df['price']
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=0.3, random_state= 0)
from sklearn.linear_model import LinearRegression
LR = LinearRegression()
LR.fit(x_train, y_train)
print(LR.score(x_train,y_train))
print(LR.score(x_test,y_test))
#%%
#1.2 Cross Validation(Ini buat nentuin berapa persen jadi training berapa persen jadi testing)
from sklearn.model_selection import cross_val_score, cross_val_predict
for i in range(2,11):
    print('======================')
    CV = cross_val_score(LR, x_train, y_train, cv = i)
    print(CV.mean())
    print('======================')
#%%
#2. Cek apakah model overfit atau underfit(ngubah degree nya)
error = []
degrees = []
for i in range(1,10):
    PF = PolynomialFeatures(degree = i)
    x_train_pf = PF.fit_transform(x_train)
    x_test_pf = PF.fit_transform(x_test)
    LR.fit(x_train_pf, y_train)
    degrees.append(i)
    error.append(np.mean(cross_val_score(LR, x_train_pf, y_train,cv = 3)))
#%%
sns.lineplot(x= degrees, y = error)
#3. Ridge (Digunakan jika terdapat outlier/ overfit)
#4. Grid Search (Untuk ngecek alpha yang tepat untuk ridge regression berapa)
# %%
print("Head")
print("Fire Breathing Dragon")
print("Dragon head")
# %%
