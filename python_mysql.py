#%%
#pip install mysql-connector-python Jika belum memiliki modulnya
#import modulenya
import mysql.connector

#%%
#Membuat koneksi
mydb = mysql.connector.connect(
    
)
print(mydb)

#%%
#Membuat Cursor(Untuk melakukan query)
mycursor = mydb.cursor()

#%%
#Membuat database
mycursor.execute("CREATE DATABASE q")

#%%
#Nunjukkin Database
mycursor.execute("SHOW DATABASES")
for x in mycursor:
  print(x)
  print(x)

#%%
#Mengubah table (Nambah atau ngurang)
sql = "INSERT INTO customers (name, address) VALUES (%s,%s)"
val = ("John", "Highway 21")
#Jika ingin multiple row val = [(),(),(),(),()], mycursor.executemany(sql,val)
mycursor.execute(sql,val)
mydb.commit()

#%%
#Menunjukkan table
mycursor.execute("SHOW TABLES")
for x in mycursor:
    print(x)

#%%
#Melakukan Select
mycursor.execute("SELECT * FROM products")
myresult = mycursor.fetchall()
#jika ingin satu baris gunakan fetchone()

#%%
#Mengubah menjadi dataframe
import pandas as pd
df = pd.DataFrame(myresult)
print(df.head())
# %%
"""
*Yang butuh commit Update, Delete, Insert (DML)
Attributes
.lastrowid = Untuk mengambil ID baris terakhir
.rowcount = menghitung jumlah baris yang diubah
"""
# %%
